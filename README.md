# Integración con Pacioli #

## Proyectos de Ejemplo ##

Integración con Pacioli Facturación Electrónica.

# Antes de Comenzar

* Instalar Pacioli Facturación Electrónica normalmente en el mismo servidor
* Registrar objeto COM

```
#!cmd

cd "C:\Program Files (x86)\Pacioli Facturación"
pacioli.exe /regserver
```