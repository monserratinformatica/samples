﻿using System;
using System.Windows.Forms;
using PacioliSrv;
using System.Collections.Generic;
using System.IO;

namespace WindowsFormsApplication1
{

    public partial class Form1 : Form
    {
        private PacioliCOMObj _objPacioli = null;
        private string baseDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Monserrat";
        private PacioliCOMObj objPacioli
        {
            get
            {
                if (_objPacioli == null)
                {
                    _objPacioli = new PacioliCOMObj();
                }
                return _objPacioli;
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Crear una factura
            IPacioliFacturaElectronica factura = objPacioli.NuevaFacturaElectronica();

            // Datos del receptor
            factura.Receptor.Rut = 12345678;
            factura.Receptor.RazonSocial = "Cliente de Prueba";
            factura.Receptor.Direccion = "Avenida X 123";
            factura.Receptor.Comuna = "Huechuraba";
            factura.Receptor.Ciudad = "Santiago";
            factura.Receptor.Giro = "Otro";

            // Agregar items - a manera de ejemplo, creamos 10 ítems
            for (int i = 0; i < 10; i++)
            {
                var item = factura.NuevaLineaDetalle();
                item.NombreItem = "Item " + i.ToString();
                item.Precio = 100;
                item.Cantidad = 1;
                item.Monto = (decimal)(item.Precio * (decimal)item.Cantidad);
                item.TipoCodigo = "INTERNO";
                item.ValorCodigo = String.Format("COD{0,010}", i);
                item.UnidadMedida = "un";
                item.Exento = false;
                var imp = item.NuevoImpuesto();
                imp.Codigo = 271;
                imp.Tasa = 18;
            }

            IPacioliDocumentoReferencia referencia = factura.NuevoDocumentoReferencia();
            referencia.FechaReferencia = DateTime.Now;
            referencia.TipoDocumento = "33"; // Texto libre, pero cuando es un código SII el valor del folio debe estar correcto
            referencia.Folio = "123";
            referencia.RazonReferencia = "Motivo de la referencia";

            string msg;
            // Emitir factura como documento electrónico
            objPacioli.EmitirDTE(factura, out msg);

            // Si el documento fue emitido correctamente el objeto tendrá asignado un número
            // de folio mayor a cero
            if (factura.Folio > 0)
            {
                // El identificador de envío sirve en caso de querer revisar la factura en el SII
                MessageBox.Show("Factura emitida - Folio: " + factura.Folio + " - Identificador envío: " + factura.NumeroEnvio);

                // Grabamos PDF y luego lo mostramos usando la aplicación del sistema
                string fileName = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";
                if (objPacioli.ExportarPDF(33, factura.Folio, String.Format(fileName, factura.Folio), true, out msg) == 0)
                {
                    System.Diagnostics.Process.Start(fileName);
                }
            }
            else
            {
                MessageBox.Show("La factura no pudo ser emitida: " + msg);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            string[] dirs = Directory.GetDirectories(baseDir);

            foreach (string dir in dirs)
            {
                if (File.Exists(dir + "\\pacioli.cfg"))
                {
                    comboBox1.Items.Add(dir.Replace(baseDir + "\\", ""));
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var Msg = "";
            button1.Enabled = textBox1.Enabled = textBox2.Enabled = comboBox1.Enabled = false;
            // Creación de objeto e inicio de sesión en objeto global
            if (objPacioli.IniciarSesionEx(textBox1.Text, textBox2.Text, baseDir + "\\" + comboBox1.Text, out Msg) == true)
            {
                button2.Enabled = true;
            }
            else
            {
                button1.Enabled = textBox1.Enabled = textBox2.Enabled = comboBox1.Enabled = true;
                MessageBox.Show("No se puede iniciar sesión: " + Msg);
            }
        }
    }
}
