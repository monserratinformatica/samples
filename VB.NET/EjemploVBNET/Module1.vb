﻿Imports PacioliSrv

Module Module1
    Sub Main()
        Dim objPacioli As IPacioliCOMObj
        Dim u, p, d, msg

        objPacioli = CreateObject("PacioliSrv.PacioliCOMObj")
        u = "USERNAME"
        p = "PASSWORD"
        d = Environ("APPDATA") & "\Monserrat\Default" ' Verificar que el path sea el correcto
        msg = ""

        Console.WriteLine("Iniciando sesión usando path " & d)
        If objPacioli.IniciarSesion(u, p, d) Then
            Dim factura As IPacioliFacturaElectronica
            factura = objPacioli.NuevaFacturaElectronica

            factura.Receptor.RazonSocial = "Cliente de Prueba Ltda."
            factura.Receptor.Rut = 88888888
            factura.Receptor.Direccion = "Avenida de Prueba 123"
            factura.Receptor.Comuna = "Santiago"
            factura.Receptor.Ciudad = "Santiago"
            factura.Receptor.Giro = "Giro de Prueba"

            Dim i As Integer
            For i = 1 To 5
                Dim linea As IPacioliLineaDetalle
                linea = factura.NuevaLineaDetalle
                linea.NombreItem = "Detalle " & i
                linea.Precio = 1000 * i
                linea.Cantidad = 1
                linea.Monto = linea.Precio * linea.Cantidad
                linea.TipoCodigo = "INTERNO"
                linea.ValorCodigo = ""
                linea.UnidadMedida = "un"
                linea.Exento = True
            Next i

            ' Referencias
            Dim referencia As IPacioliDocumentoReferencia

            referencia = factura.NuevoDocumentoReferencia
            referencia.FechaReferencia = New DateTime(2015, 11, 10)
            referencia.TipoDocumento = "801" ' Texto libre, pero cuando es un código SII el valor del folio debe estar correcto
            referencia.Folio = "123"
            referencia.RazonReferencia = "Cotización"
            objPacioli.EmitirDTE(factura, msg)

            If factura.Folio32 > 0 Then
                Console.WriteLine("Factura emitida - Folio: " & CStr(factura.Folio32) & " - Identificador envío: " & CStr(factura.NumeroEnvio))
                Dim filename As String
                filename = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\factura-" & CStr(factura.Folio32) & ".pdf"
                objPacioli.ExportarPDF32(33, factura.Folio32, filename, True, msg)
                If msg <> "" Then
                    Console.WriteLine("Mensaje: " & msg)
                Else
                    Console.WriteLine("Documento exportado: " & filename)
                End If
            Else
                Console.WriteLine("Factura no se pudo emitir:  " + msg)
            End If
        End If
        objPacioli.FinalizarSesion()
        Console.ReadKey()
    End Sub
End Module
