VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Emisi�n de DTE con Pacioli"
   ClientHeight    =   4065
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   6015
   LinkTopic       =   "Form1"
   ScaleHeight     =   4065
   ScaleWidth      =   6015
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   2640
      TabIndex        =   9
      Text            =   "0"
      Top             =   3240
      Width           =   2895
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Emitir Nota p/ Boleta Electr�nica"
      Height          =   495
      Left            =   120
      TabIndex        =   8
      Top             =   3120
      Width           =   2295
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Emitir Gu�a"
      Height          =   495
      Left            =   3120
      TabIndex        =   7
      Top             =   2400
      Width           =   2175
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Emitir Factura"
      Height          =   495
      Left            =   960
      TabIndex        =   6
      Top             =   2400
      Width           =   2055
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      ItemData        =   "Form1.frx":0000
      Left            =   2520
      List            =   "Form1.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   1440
      Width           =   3135
   End
   Begin VB.TextBox editPassword 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   2520
      PasswordChar    =   "*"
      TabIndex        =   4
      Top             =   1080
      Width           =   3135
   End
   Begin VB.TextBox editUsername 
      Height          =   285
      Left            =   2520
      TabIndex        =   3
      Top             =   720
      Width           =   3135
   End
   Begin VB.Label Label2 
      Caption         =   "Configuraci�n"
      Height          =   375
      Left            =   360
      TabIndex        =   2
      Top             =   1440
      Width           =   2055
   End
   Begin VB.Label Label3 
      Caption         =   "Contrase�a"
      Height          =   255
      Left            =   360
      TabIndex        =   1
      Top             =   1080
      Width           =   2415
   End
   Begin VB.Label Label1 
      Caption         =   "Nombre de Usuario"
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   720
      Width           =   2055
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

Function ConfiguracionesPacioli()
    Dim fso As Scripting.FileSystemObject
    Set fso = CreateObject("Scripting.FileSystemObject")
    Dim fld As Folder
    Dim d As Folder
    Dim counter As Integer
    Dim arr() As String
    
    counter = 0
    Set fld = fso.GetFolder(Environ("APPDATA") & "\Monserrat")
    For Each d In fld.SubFolders
        If fso.FileExists(d & "\pacioli.cfg") Then
            ReDim Preserve arr(counter)
            arr(counter) = d.Name
            counter = counter + 1
        End If
    Next
    ConfiguracionesPacioli = arr
    Set d = Nothing
    Set fld = Nothing
    Set fso = Nothing
End Function

Sub FacturarConPacioli(username As String, password As String, config As String)
    Dim objPacioli As PacioliSrv.PacioliCOMObj
    Set objPacioli = CreateObject("PacioliSrv.PacioliCOMObj")

    Dim u
    Dim p
    Dim d
    Dim Msg As String
    

    u = username
    p = password
    d = Environ("APPDATA") & "\Monserrat\" & config

    If objPacioli.IniciarSesion(u, p, d) Then
        Dim factura As IPacioliFacturaElectronica
        Set factura = objPacioli.NuevaFacturaElectronica
        
        factura.Receptor.RazonSocial = "Cliente de Prueba Ltda."
        factura.Receptor.Rut = 88888888
        factura.Receptor.Direccion = "Avenida de Prueba 123"
        factura.Receptor.Comuna = "Santiago"
        factura.Receptor.Ciudad = "Santiago"
        factura.Receptor.Giro = "Giro de Prueba"
        
        Dim i As Integer
        ' For i = 1 To 5
        '    Dim linea As IPacioliLineaDetalle
        '    Set linea = factura.NuevaLineaDetalle
        '    linea.NombreItem = "Detalle " & i
        '    linea.Precio = 1000 * i
        '    linea.Cantidad = 1
        '    linea.Monto = linea.Precio * linea.Cantidad
        '    linea.TipoCodigo = "INTERNO"
        '    linea.ValorCodigo = ""
        '    linea.UnidadMedida = "un"
        ' Next i
        
        Dim linea As IPacioliLineaDetalle
        Set linea = factura.NuevaLineaDetalle
        linea.NombreItem = "Coca Cola"
        linea.Precio = 729.1971
        linea.Cantidad = 2
        linea.TipoCodigo = "INTERNO"
        linea.ValorCodigo = ""
        linea.UnidadMedida = "lts"
        
        Dim impuesto As IPacioliImpuesto
        Set impuesto = linea.NuevoImpuesto
        impuesto.Codigo = 271
        impuesto.Tasa = 18
        
        objPacioli.EmitirDTE factura, Msg
        If factura.Folio32 > 0 Then
            MsgBox "Factura emitida - Folio: " & CStr(factura.Folio32) & " - Identificador env�o: " & CStr(factura.NumeroEnvio)
            Dim filename As String
            
            filename = Environ("TEMP") & "\factura-" & CStr(factura.Folio32) & ".pdf"
            
            objPacioli.ExportarPDF32 33, factura.Folio32, filename, True, Msg
            ShellExecute hWnd, "open", filename, vbNullString, vbNullString, 1
        Else
            MsgBox "Factura no se pudo emitir: " + Msg
        End If
    End If
    objPacioli.FinalizarSesion
End Sub

Sub EmitirGuiaConPacioli(username As String, password As String, config As String)
    Dim objPacioli As PacioliSrv.PacioliCOMObj
    Set objPacioli = CreateObject("PacioliSrv.PacioliCOMObj")

    Dim u
    Dim p
    Dim d
    Dim Msg As String
    

    u = username
    p = password
    d = Environ("APPDATA") & "\Monserrat\" & config

    If objPacioli.IniciarSesion(u, p, d) Then
        Dim guia As IPacioliGuiaDespachoElectronica
        
        Set guia = objPacioli.NuevaGuiaDespachoElectronica
                
        guia.Receptor.RazonSocial = "Cliente de Prueba Ltda."
        guia.Receptor.Rut = 88888888
        guia.Receptor.Direccion = "Avenida de Prueba 123"
        guia.Receptor.Comuna = "Santiago"
        guia.Receptor.Ciudad = "Santiago"
        guia.Receptor.Giro = "Giro de Prueba"
        
        ' Motivo Gu�a
        ' 1: Operaci�n constituye venta
        ' 2: Ventas por efectuar
        ' 3: Consignaciones
        ' 4: Entrega gratuita
        ' 5: traslados internos
        ' 6: Otros traslados no Venta
        ' 7: Gu�a de devoluci�n
        ' 8: Traslado para exportaci�n. (no venta)
        ' 9: Venta para exportaci�n
        guia.MotivoGuia = 1
        
        ' Tipo de despacho
        ' 1: Despacho por cuenta del receptor del documento (cliente o vendedor en caso de Facturas de compra.)
        ' 2: Despacho por cuenta del emisor a instalaciones del cliente
        ' 3: Despacho por cuenta del emisor a otras instalaciones (Ejemplo: entrega en Obra)
        guia.TipoDespacho = 1
        
        ' L�neas de detalle
        Dim i As Integer
        For i = 1 To 5
            Dim linea As IPacioliLineaDetalle
            Set linea = guia.NuevaLineaDetalle
            linea.NombreItem = "Detalle " & i
            linea.Precio = 1000 * i
            linea.Cantidad = 1
            linea.Monto = linea.Precio * linea.Cantidad
            linea.TipoCodigo = "INTERNO"
            linea.ValorCodigo = ""
            linea.UnidadMedida = "un"
        Next i
        
        ' Referencias
        Dim referencia As IPacioliDocumentoReferencia
        Set referencia = guia.NuevoDocumentoReferencia
        referencia.FechaReferencia = DateTime.DateValue("2015-11-01")
        referencia.TipoDocumento = "33" ' Texto libre, pero cuando es un c�digo SII el valor del folio debe estar correcto
        referencia.Folio = "123"
        referencia.RazonReferencia = "Motivo de la referencia"
        
        
        objPacioli.EmitirDTE guia, Msg
        
        If guia.Folio32 > 0 Then
            MsgBox "Factura emitida - Folio: " & CStr(guia.Folio32) & " - Identificador env�o: " & CStr(guia.NumeroEnvio)
            Dim filename As String
            
            filename = Environ("TEMP") & "\factura-" & CStr(guia.Folio32) & ".pdf"
            
            objPacioli.ExportarPDF32 33, guia.Folio32, filename, True, Msg
            ShellExecute hWnd, "open", filename, vbNullString, vbNullString, 1
        Else
            MsgBox "Gu�a no se pudo emitir: " + Msg
        End If
    End If
    objPacioli.FinalizarSesion
End Sub

Sub EmitirNotaCreditoBoleta(username As String, password As String, config As String, numeroBoleta As Integer)
    Dim objPacioli As PacioliSrv.PacioliCOMObj
    Set objPacioli = CreateObject("PacioliSrv.PacioliCOMObj")

    Dim u
    Dim p
    Dim d
    Dim Msg As String

    u = username
    p = password
    d = Environ("APPDATA") & "\Monserrat\" & config

    If objPacioli.IniciarSesion(u, p, d) Then
        Dim nota As IPacioliNotaCreditoElectronica
                
        Set nota = objPacioli.NuevaNotaCreditoElectronica
                        
        nota.Receptor.RazonSocial = "Cliente de Prueba Ltda."
        nota.Receptor.Rut = 88888888
        nota.Receptor.Direccion = "Avenida de Prueba 123"
        nota.Receptor.Comuna = "Santiago"
        nota.Receptor.Ciudad = "Santiago"
        nota.Receptor.Giro = "Giro de Prueba"
        
       
        ' 1: Anula Documento de referencia
        ' 2: Corrige Texto Documento de referencia
        ' 3: Corrige montos
        nota.MontosBrutos = True
        nota.MotivoNota = 1
        nota.TipoDocRef = 39 ' 35 = Boletas y Comprobante Fiscal, 39 = Boleta Electr�nica
        nota.FolioRef32 = numeroBoleta
        nota.FechaReferencia = Now ' usar fecha del documento referido
         
        ' L�neas de detalle - si anula debe coincidir con el monto anulado
        Dim i As Integer
        For i = 1 To 5
            Dim linea As IPacioliLineaDetalle
            Set linea = nota.NuevaLineaDetalle
            linea.NombreItem = "Detalle " & i
            linea.Precio = 1000 * i
            linea.Cantidad = 1
            linea.Monto = linea.Precio * linea.Cantidad
            linea.TipoCodigo = "INTERNO"
            linea.ValorCodigo = ""
            linea.UnidadMedida = "un"
        Next i
        
        objPacioli.EmitirDTE nota, Msg
        
        If nota.Folio32 > 0 Then
            MsgBox "Factura emitida - Folio: " & CStr(nota.Folio32) & " - Identificador env�o: " & CStr(nota.NumeroEnvio)
            Dim filename As String
            
            filename = Environ("TEMP") & "\factura-" & CStr(nota.Folio32) & ".pdf"
            
            objPacioli.ExportarPDF32 61, nota.Folio32, filename, True, Msg
            ShellExecute hWnd, "open", filename, vbNullString, vbNullString, 1
        Else
            MsgBox "No se pudo emitir nota de cr�dito: " + Msg
        End If
    End If
    objPacioli.FinalizarSesion
End Sub


Private Sub Command1_Click()
    EmitirGuiaConPacioli editUsername.Text, editPassword.Text, Combo1.Text
End Sub

Private Sub Command2_Click()
    FacturarConPacioli editUsername.Text, editPassword.Text, Combo1.Text
End Sub


Private Sub Command3_Click()
    EmitirNotaCreditoBoleta editUsername.Text, editPassword.Text, Combo1.Text, CInt(Text1.Text)
End Sub

Private Sub Form_Load()
    Dim PacioliConfigs ' () As String
    Dim i As Integer
   
    PacioliConfigs = ConfiguracionesPacioli()
    i = UBound(PacioliConfigs) - LBound(PacioliConfigs) + 1
    
    If i <= 0 Then
        MsgBox "No se encontraron configuraciones activas de Pacioli en este equipo"
        Return
    End If
    
    If i > 0 Then
        Dim cfg
        For Each cfg In PacioliConfigs
            Combo1.AddItem cfg
        Next
        Combo1.ListIndex = 0
    End If
End Sub

