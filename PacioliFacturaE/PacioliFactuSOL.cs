﻿using PacioliSrv;
using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace PacioliFactuSOL
{

    public partial class FormPacioliFactuSOL : Form
    {
        private PacioliCOMObj _objPacioli = null;
        private string baseDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Monserrat";
        private PacioliCOMObj objPacioli
        {
            get
            {
                if (_objPacioli == null)
                {
                    _objPacioli = new PacioliCOMObj();
                }
                return _objPacioli;
            }
        }

        public FormPacioliFactuSOL()
        {
            InitializeComponent();
        }

        private int rutNumerico(string rut)
        {
            var parteNumerica = 0;
            try
            {
                rut = rut.Trim().ToUpper();
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                var rutAux = int.Parse(rut.Substring(0, rut.Length - 1));
                parteNumerica = rutAux;

                char dv = char.Parse(rut.Substring(rut.Length - 1, 1));

                int m = 0, s = 1;
                for (; rutAux != 0; rutAux /= 10)
                {
                    s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                }
                if (dv != (char)(s != 0 ? s + 47 : 75))
                {
                    parteNumerica = 0;
                }
            }
            catch (Exception)
            {
                
            }
            return parteNumerica;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] dirs = Directory.GetDirectories(baseDir);

            foreach (string dir in dirs)
            {
                if (File.Exists(dir + "\\pacioli.cfg"))
                {
                    cbConfiguracionesActivas.Items.Add(dir.Replace(baseDir + "\\", ""));
                }
            }
        }

        private void iniciarSesion(object sender, EventArgs e)
        {
            var Msg = "";
            btnIniciarSesion.Enabled = editUsername.Enabled = editPassword.Enabled = cbConfiguracionesActivas.Enabled = false;
            // Creación de objeto e inicio de sesión en objeto global
            if (objPacioli.IniciarSesionEx(editUsername.Text, editPassword.Text, baseDir + "\\" + cbConfiguracionesActivas.Text, out Msg) == true)
            {
                btnAbrirArchivo.Enabled = true;
            }
            else
            {
                btnIniciarSesion.Enabled = editUsername.Enabled = editPassword.Enabled = cbConfiguracionesActivas.Enabled = true;
                MessageBox.Show("No se puede iniciar sesión: " + Msg);
            }
        }

        private Facturae facturae = null;

        private void leerFacturae(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                var xRoot = new XmlRootAttribute();
                xRoot.ElementName = "Facturae";
                xRoot.Namespace = @"http://www.facturae.es/Facturae/2009/v3.2/Facturae";
                xRoot.IsNullable = true;
                using (var reader = XmlReader.Create(openFileDialog1.FileName))
                {
                    var serializer = new XmlSerializer(typeof(Facturae), xRoot);
                    try {
                        facturae = (Facturae)serializer.Deserialize(reader);
                        editArchivo.Text = openFileDialog1.FileName;
                        btnEmitirDTE.Enabled = true;
                    } catch(Exception exception)
                    {
                        lblMensajes.Text = exception.Message;
                        facturae = null;
                    }
                }                
            }
        }

        private void generarDTE(object sender, EventArgs e)
        {
            btnEmitirDTE.Enabled = false;
            if (facturae == null)
            {
                MessageBox.Show("Debe especificar un archivo FacturaE válido.");
                return;
            }

            if (facturae.Invoices.Length != 1)
            {
                MessageBox.Show(string.Format("De momento sólo podemos facturar archivos con una factura (archivo contiene {0} facturas)", facturae.Invoices.Length));
                return;
            }

            var receptor = facturae.Parties.BuyerParty;
            var inv = facturae.Invoices[0];
            // Crear una factura
            IPacioliFacturaElectronica factura = objPacioli.NuevaFacturaElectronica();

            var ind = (receptor.Item as IndividualType);
            if (ind == null)
            {
                MessageBox.Show("Sólo se pueden usar facturas con party especificado como individuo");
                return;
            }

            var dir = (ind.Item as AddressType); // AddressInSpain
            if (dir == null)
            {
                MessageBox.Show("Sólo se pueden usar facturas con AddressInSpain");
                return;
            }

            // Datos del receptor
            factura.Receptor.Rut = rutNumerico(receptor.TaxIdentification.TaxIdentificationNumber);
            factura.Receptor.RazonSocial = ind.Name;
            factura.Receptor.Direccion = dir.Address;
            factura.Receptor.Comuna = dir.Town;
            factura.Receptor.Ciudad = dir.Province ?? dir.Town;
            factura.Receptor.Giro = "Otro";

            var referencia = factura.NuevoDocumentoReferencia();
            referencia.TipoDocumento = "FACTURAE";
            referencia.Folio = inv.InvoiceHeader.InvoiceNumber;
            referencia.FechaReferencia = inv.InvoiceIssueData.IssueDate;
            referencia.RazonReferencia = "Referencia Interna";

            // Agregar items - a manera de ejemplo, creamos 10 ítems
            for (int i = 0; i < inv.Items.Length; i++)
            {
                var linea = inv.Items[i];
                var item = factura.NuevaLineaDetalle();
                item.NombreItem = linea.ItemDescription;
                item.Precio = (decimal)linea.UnitPriceWithoutTax;
                item.Cantidad = linea.Quantity;
                item.Monto = item.Precio * (decimal)item.Cantidad;
                item.TipoCodigo = "OTRO";
                item.ValorCodigo = linea.ArticleCode;
                item.UnidadMedida = linea.UnitOfMeasureSpecified ? linea.UnitOfMeasure.ToString() : "un";
                item.Exento = linea.TaxesOutputs == null || linea.TaxesOutputs.Length == 0;
            }

            string msg;
            // Emitir factura como documento electrónico
            objPacioli.EmitirDTE(factura, out msg);

            // Si el documento fue emitido correctamente el objeto tendrá asignado un número
            // de folio mayor a cero
            if (factura.Folio > 0)
            {
                // El identificador de envío sirve en caso de querer revisar la factura en el SII
                lblMensajes.Text = "Factura emitida - Folio: " + factura.Folio + " - Identificador envío: " + factura.NumeroEnvio;
                // Grabamos PDF y luego lo mostramos usando la aplicación del sistema
                string fileName = Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";
                if (objPacioli.ExportarPDF(33, factura.Folio, string.Format(fileName, factura.Folio), true, out msg) == 0)
                {
                    System.Diagnostics.Process.Start(fileName);
                } else
                {
                    lblMensajes.Text = msg;
                }
            }
            else
            {
                MessageBox.Show("La factura no pudo ser emitida: " + msg);
                lblMensajes.Text = msg;
            }
        }
    }
}
