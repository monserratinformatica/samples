﻿namespace PacioliFactuSOL
{
    partial class FormPacioliFactuSOL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnIniciarSesion = new System.Windows.Forms.Button();
            this.cbConfiguracionesActivas = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.editUsername = new System.Windows.Forms.TextBox();
            this.editPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.editArchivo = new System.Windows.Forms.TextBox();
            this.btnAbrirArchivo = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnEmitirDTE = new System.Windows.Forms.Button();
            this.lblMensajes = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnIniciarSesion
            // 
            this.btnIniciarSesion.Location = new System.Drawing.Point(407, 12);
            this.btnIniciarSesion.Name = "btnIniciarSesion";
            this.btnIniciarSesion.Size = new System.Drawing.Size(85, 26);
            this.btnIniciarSesion.TabIndex = 3;
            this.btnIniciarSesion.Text = "Iniciar sesión";
            this.btnIniciarSesion.UseVisualStyleBackColor = true;
            this.btnIniciarSesion.Click += new System.EventHandler(this.iniciarSesion);
            // 
            // cbConfiguracionesActivas
            // 
            this.cbConfiguracionesActivas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConfiguracionesActivas.FormattingEnabled = true;
            this.cbConfiguracionesActivas.Location = new System.Drawing.Point(121, 12);
            this.cbConfiguracionesActivas.Name = "cbConfiguracionesActivas";
            this.cbConfiguracionesActivas.Size = new System.Drawing.Size(217, 21);
            this.cbConfiguracionesActivas.Sorted = true;
            this.cbConfiguracionesActivas.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Configuración";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Nombre de Usuario";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Contraseña";
            // 
            // editUsername
            // 
            this.editUsername.Location = new System.Drawing.Point(121, 39);
            this.editUsername.Name = "editUsername";
            this.editUsername.Size = new System.Drawing.Size(100, 20);
            this.editUsername.TabIndex = 8;
            // 
            // editPassword
            // 
            this.editPassword.Location = new System.Drawing.Point(121, 65);
            this.editPassword.Name = "editPassword";
            this.editPassword.PasswordChar = '*';
            this.editPassword.Size = new System.Drawing.Size(100, 20);
            this.editPassword.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Location = new System.Drawing.Point(12, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Archivo FacturaE";
            // 
            // editArchivo
            // 
            this.editArchivo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editArchivo.Enabled = false;
            this.editArchivo.Location = new System.Drawing.Point(121, 91);
            this.editArchivo.Name = "editArchivo";
            this.editArchivo.ReadOnly = true;
            this.editArchivo.Size = new System.Drawing.Size(280, 20);
            this.editArchivo.TabIndex = 11;
            // 
            // btnAbrirArchivo
            // 
            this.btnAbrirArchivo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAbrirArchivo.Enabled = false;
            this.btnAbrirArchivo.Location = new System.Drawing.Point(407, 89);
            this.btnAbrirArchivo.Name = "btnAbrirArchivo";
            this.btnAbrirArchivo.Size = new System.Drawing.Size(85, 23);
            this.btnAbrirArchivo.TabIndex = 12;
            this.btnAbrirArchivo.Text = "Abrir...";
            this.btnAbrirArchivo.UseVisualStyleBackColor = true;
            this.btnAbrirArchivo.Click += new System.EventHandler(this.leerFacturae);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnEmitirDTE
            // 
            this.btnEmitirDTE.Enabled = false;
            this.btnEmitirDTE.Location = new System.Drawing.Point(407, 132);
            this.btnEmitirDTE.Name = "btnEmitirDTE";
            this.btnEmitirDTE.Size = new System.Drawing.Size(85, 23);
            this.btnEmitirDTE.TabIndex = 13;
            this.btnEmitirDTE.Text = "Emitir DTE";
            this.btnEmitirDTE.UseVisualStyleBackColor = true;
            this.btnEmitirDTE.Click += new System.EventHandler(this.generarDTE);
            // 
            // lblMensajes
            // 
            this.lblMensajes.Location = new System.Drawing.Point(13, 126);
            this.lblMensajes.Name = "lblMensajes";
            this.lblMensajes.Size = new System.Drawing.Size(375, 29);
            this.lblMensajes.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 171);
            this.Controls.Add(this.lblMensajes);
            this.Controls.Add(this.btnEmitirDTE);
            this.Controls.Add(this.btnAbrirArchivo);
            this.Controls.Add(this.editArchivo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.editPassword);
            this.Controls.Add(this.editUsername);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbConfiguracionesActivas);
            this.Controls.Add(this.btnIniciarSesion);
            this.Name = "Form1";
            this.Text = "Generar DTE desde archivo Facturae";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnIniciarSesion;
        private System.Windows.Forms.ComboBox cbConfiguracionesActivas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox editUsername;
        private System.Windows.Forms.TextBox editPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox editArchivo;
        private System.Windows.Forms.Button btnAbrirArchivo;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnEmitirDTE;
        private System.Windows.Forms.Label lblMensajes;
    }
}

