unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PacioliSrv_TLB, StdCtrls;

type
  TForm1 = class(TForm)
    cbConfiguracionesActivas: TComboBox;
    lblPerfil: TLabel;
    editUsername: TEdit;
    editPassword: TEdit;
    lblUsername: TLabel;
    lblPassword: TLabel;
    btnIniciaSesion: TButton;
    btnEmitirFactura: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cbConfiguracionesActivasChange(Sender: TObject);
    procedure btnIniciaSesionClick(Sender: TObject);
    procedure btnEmitirFacturaClick(Sender: TObject);
  private
    FObjPacioli: IPacioliCOMObj;
    procedure CargarDirectoriosPacioli;
  public

  end;

var
  Form1: TForm1;

implementation

uses IOUtils, ShellAPI;

type
  TString = class(TObject)
  private
    fStr: String;
  public
    constructor Create(const AStr: String);
    property Str: String read fStr write fStr;
  end;

{$R *.dfm}

constructor TString.Create(const AStr: String);
begin
  inherited Create;
  fStr := AStr;
end;

procedure TForm1.btnEmitirFacturaClick(Sender: TObject);
var
  factura: IPacioliFacturaElectronica;
  item: IPacioliLineaDetalle;
  i: Integer;
  msg: WideString;
  tempPath: TFileName;
begin
  btnEmitirFactura.Enabled := false;
  try
    factura := FObjPacioli.NuevaFacturaElectronica;
    factura.Receptor.Rut := 88888888;
    factura.Receptor.RazonSocial := 'Cliente de Prueba';
    factura.Receptor.Direccion := 'Avenida X 123';
    factura.Receptor.Comuna := 'Huechuraba';
    factura.Receptor.Ciudad := 'Santiago';
    factura.Receptor.Giro := 'Otro';

    for i := 0 to 9 do
    begin
      item := factura.NuevaLineaDetalle();
      item.NombreItem := ' item ' + IntToStr(i);
      item.Precio := 100;
      item.Cantidad := 1;
      item.Monto := item.Precio * item.Cantidad;
      item.TipoCodigo := ' INTERNO ';
      item.ValorCodigo := Format(' COD %010d ', [i]);
      item.UnidadMedida := ' un ';
      item.Exento := false;
    end;

    FObjPacioli.EmitirDTE(factura, msg);
    // Si el documento fue emitido correctamente el objeto tendr� asignado un n�mero
    // de folio mayor a cero
    if (factura.Folio > 0) then
    begin
      ShowMessage('Factura emitida - Folio: ' + IntToStr(factura.Folio) +
        ' - Identificador env�o: ' + IntToStr(factura.NumeroEnvio));
      tempPath := IOUtils.TPath.ChangeExtension(TPath.GetTempFileName, 'pdf');
      if FObjPacioli.ExportarPDF(33, factura.Folio, tempPath, false,
        msg) = 0 then
      begin
        ShellExecute(Self.Handle, 'open', PChar(tempPath), '', '',
          SW_SHOWNORMAL);
      end;
    end;
  finally
    btnEmitirFactura.Enabled := true;
  end;
end;

procedure TForm1.btnIniciaSesionClick(Sender: TObject);
var
  configuracionActiva, wUsername, wPassword, msg: WideString;
begin
  configuracionActiva := (cbConfiguracionesActivas.Items.Objects
    [cbConfiguracionesActivas.ItemIndex] as TString).Str;
  wUsername := editUsername.Text;
  wPassword := editPassword.Text;

  if FObjPacioli.IniciarSesionEx(wUsername, wPassword, configuracionActiva,
    msg) then
  begin
    btnIniciaSesion.Enabled := false;
    btnEmitirFactura.Enabled := true;
    cbConfiguracionesActivas.Enabled := false;
    editUsername.Enabled := false;
    editPassword.Enabled := false;
  end
  else
  begin
    ShowMessage('Error iniciando sesi�n: ' + msg);
  end;
end;

procedure TForm1.CargarDirectoriosPacioli;
var
  homePath: String;
  SR: TSearchRec;
  candidato: string;
begin
  homePath := IncludeTrailingPathDelimiter(IOUtils.TPath.GetHomePath) +
    'Monserrat\';
  if FindFirst(homePath + '*.*', faDirectory, SR) = 0 then
  begin
    repeat
      candidato := homePath + IncludeTrailingPathDelimiter(SR.Name);
      if FileExists(candidato + 'licencia.xml') then
        cbConfiguracionesActivas.Items.AddObject(SR.Name,
          TString.Create(candidato));
    until FindNext(SR) <> 0;
  end;
end;

procedure TForm1.cbConfiguracionesActivasChange(Sender: TObject);
begin
  btnIniciaSesion.Enabled := (cbConfiguracionesActivas.ItemIndex >= 0);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  FObjPacioli := CoPacioliCOMObj.Create;
  CargarDirectoriosPacioli;
end;

procedure TForm1.FormDestroy(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to cbConfiguracionesActivas.Items.Count - 1 do
    cbConfiguracionesActivas.Items.Objects[i].Free;
  FObjPacioli := nil;
end;

end.
