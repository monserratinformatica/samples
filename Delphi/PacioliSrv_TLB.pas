﻿unit PacioliSrv_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 34747 $
// File generated on 04-04-2016 14:46:24 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files (x86)\Pacioli Facturación\pacioli.exe (1)
// LIBID: {769AE723-BB73-4EB9-9138-F8A93C86A195}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  PacioliSrvMajorVersion = 1;
  PacioliSrvMinorVersion = 0;

  LIBID_PacioliSrv: TGUID = '{769AE723-BB73-4EB9-9138-F8A93C86A195}';

  IID_IPacioliCOMObj: TGUID = '{5AD1D54F-31B4-4E82-93A6-9B0305E124C4}';
  CLASS_PacioliCOMObj: TGUID = '{12B96BDC-ABBB-4EA5-A7E9-9F4797C952B2}';
  IID_IPacioliDTE: TGUID = '{933FC237-5477-4DED-B71E-A979657544CC}';
  IID_IPacioliFacturaElectronica: TGUID = '{60FB27A2-9F79-4F58-9767-E7CEB8D8F2FE}';
  IID_IPacioliEmpresa: TGUID = '{182BADE1-2ED1-4CDC-B010-8A89E4382CBE}';
  IID_IPacioliLineaDetalle: TGUID = '{AE212EE8-1C0F-4192-879B-318DD74B2A56}';
  IID_IPacioliDetalle: TGUID = '{4BBB1169-670F-406A-B9FB-81F78A266F12}';
  IID_IPacioliFacturaExentaElectronica: TGUID = '{CEBA1B07-3066-4C4E-8FE5-B5B036F7867A}';
  IID_IPacioliGuiaDespachoElectronica: TGUID = '{75A5D31A-13C5-404F-8630-07EC90A90398}';
  IID_IPacioliNotaCreditoElectronica: TGUID = '{36FD0A7D-44A3-4DC4-B54B-C6025C536007}';
  IID_IPacioliImpuesto: TGUID = '{8CC23941-B1CB-4B0A-9B4B-137089628C30}';
  IID_IPacioliImpuestos: TGUID = '{FFCBAB83-588C-4BC7-86B9-ACAEA999F887}';
  IID_IPacioliEstadoDTE: TGUID = '{36DDEB2D-AD57-4506-B8B6-799ABADDA8EF}';
  IID_IPacioliDocumentoReferencia: TGUID = '{44CE42A0-0BCC-4009-B419-1F6A1E161B6A}';
  IID_IPacioliDocumentosReferencia: TGUID = '{E9732759-C1D5-448E-8B74-D159318930C2}';
  IID_IPacioliNotaDebitoElectronica: TGUID = '{C4DA045A-7330-40C4-9F08-F84CE83D5AC5}';
  IID_IPacioliFacturaCompraElectronica: TGUID = '{3D258DD8-359A-4D7C-A122-F0565F12E300}';
  IID_IPacioliListaDocumentos: TGUID = '{E661F9AC-927E-4C52-AB6C-F54B2FEC5C8B}';
  IID_IPacioliResumenDocumento: TGUID = '{005352D9-08A1-42A6-AD83-DEDBEA738592}';
  IID_IPacioliCodigosDetalle: TGUID = '{4DBE137B-BA58-470C-84A2-BFA4AB9A01CD}';
  IID_IPacioliCodigoDetalle: TGUID = '{B05C8934-6F4A-4F78-A064-262ED5767D33}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IPacioliCOMObj = interface;
  IPacioliCOMObjDisp = dispinterface;
  IPacioliDTE = interface;
  IPacioliDTEDisp = dispinterface;
  IPacioliFacturaElectronica = interface;
  IPacioliFacturaElectronicaDisp = dispinterface;
  IPacioliEmpresa = interface;
  IPacioliEmpresaDisp = dispinterface;
  IPacioliLineaDetalle = interface;
  IPacioliLineaDetalleDisp = dispinterface;
  IPacioliDetalle = interface;
  IPacioliDetalleDisp = dispinterface;
  IPacioliFacturaExentaElectronica = interface;
  IPacioliFacturaExentaElectronicaDisp = dispinterface;
  IPacioliGuiaDespachoElectronica = interface;
  IPacioliGuiaDespachoElectronicaDisp = dispinterface;
  IPacioliNotaCreditoElectronica = interface;
  IPacioliNotaCreditoElectronicaDisp = dispinterface;
  IPacioliImpuesto = interface;
  IPacioliImpuestoDisp = dispinterface;
  IPacioliImpuestos = interface;
  IPacioliImpuestosDisp = dispinterface;
  IPacioliEstadoDTE = interface;
  IPacioliEstadoDTEDisp = dispinterface;
  IPacioliDocumentoReferencia = interface;
  IPacioliDocumentoReferenciaDisp = dispinterface;
  IPacioliDocumentosReferencia = interface;
  IPacioliDocumentosReferenciaDisp = dispinterface;
  IPacioliNotaDebitoElectronica = interface;
  IPacioliNotaDebitoElectronicaDisp = dispinterface;
  IPacioliFacturaCompraElectronica = interface;
  IPacioliFacturaCompraElectronicaDisp = dispinterface;
  IPacioliListaDocumentos = interface;
  IPacioliListaDocumentosDisp = dispinterface;
  IPacioliResumenDocumento = interface;
  IPacioliResumenDocumentoDisp = dispinterface;
  IPacioliCodigosDetalle = interface;
  IPacioliCodigosDetalleDisp = dispinterface;
  IPacioliCodigoDetalle = interface;
  IPacioliCodigoDetalleDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  PacioliCOMObj = IPacioliCOMObj;


// *********************************************************************//
// Interface: IPacioliCOMObj
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5AD1D54F-31B4-4E82-93A6-9B0305E124C4}
// *********************************************************************//
  IPacioliCOMObj = interface(IDispatch)
    ['{5AD1D54F-31B4-4E82-93A6-9B0305E124C4}']
    function ExportarPDF(TipoDTE: SYSINT; FolioDTE: Int64; const OutputPath: WideString; 
                         Cedible: WordBool; out Mensaje: WideString): HResult; safecall;
    procedure ProcesarCasilla; safecall;
    function IniciarSesion(const Username: WideString; const Password: WideString; 
                           const ConfigDir: WideString): WordBool; safecall;
    procedure FinalizarSesion; safecall;
    function NuevaFacturaElectronica: IPacioliFacturaElectronica; safecall;
    procedure EmitirDTE(const Factura: IPacioliDTE; out Mensaje: WideString); safecall;
    function NuevaFacturaExentaElectronica: IPacioliFacturaExentaElectronica; safecall;
    function NuevaGuiaDespachoElectronica: IPacioliGuiaDespachoElectronica; safecall;
    function NuevaNotaCreditoElectronica: IPacioliNotaCreditoElectronica; safecall;
    function ObtenerDTE(Folio: Int64; RutEmisor: SYSINT; TipoDocumento: SYSINT; 
                        out Mensaje: WideString): IPacioliDTE; safecall;
    function VerificarDTE(TipoDTE: SYSINT; Folio: Int64; out Mensaje: WideString): IPacioliEstadoDTE; safecall;
    function NuevaNotaDebitoElectronica: IPacioliNotaDebitoElectronica; safecall;
    function NuevaFacturaCompraElectronica: IPacioliFacturaCompraElectronica; safecall;
    function ObtenerDocumentos(Desde: TDateTime; Hasta: TDateTime): IPacioliListaDocumentos; safecall;
    function ExportarXML(TipoDTE: SYSINT; FolioDTE: Int64; const OutputPath: WideString; 
                         out Mensaje: WideString): HResult; safecall;
    function ObtenerEmisor(Rut: SYSINT; out Mensaje: WideString): IPacioliEmpresa; safecall;
    function ExportarPDF32(TipoDTE: SYSINT; FolioDTE: SYSINT; const OutputPath: WideString; 
                           Cedible: WordBool; out Mensaje: WideString): HResult; safecall;
    function ObtenerDTE32(Folio: SYSINT; RutEmisor: SYSINT; TipoDocumento: SYSINT; 
                          out Mensaje: WideString): IPacioliDTE; safecall;
    function VerificarDTE32(TipoDTE: SYSINT; Folio: SYSINT; out Mensaje: WideString): IPacioliEstadoDTE; safecall;
    function ExportarXML32(TipoDTE: SYSINT; FolioDTE: SYSINT; const OutputPath: WideString; 
                           out Mensaje: WideString): HResult; safecall;
    function IniciarSesionEx(const Username: WideString; const Password: WideString; 
                             const ConfigDir: WideString; out Msg: WideString): WordBool; safecall;
  end;

// *********************************************************************//
// DispIntf:  IPacioliCOMObjDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5AD1D54F-31B4-4E82-93A6-9B0305E124C4}
// *********************************************************************//
  IPacioliCOMObjDisp = dispinterface
    ['{5AD1D54F-31B4-4E82-93A6-9B0305E124C4}']
    function ExportarPDF(TipoDTE: SYSINT; FolioDTE: {??Int64}OleVariant; 
                         const OutputPath: WideString; Cedible: WordBool; out Mensaje: WideString): HResult; dispid 201;
    procedure ProcesarCasilla; dispid 202;
    function IniciarSesion(const Username: WideString; const Password: WideString; 
                           const ConfigDir: WideString): WordBool; dispid 203;
    procedure FinalizarSesion; dispid 204;
    function NuevaFacturaElectronica: IPacioliFacturaElectronica; dispid 205;
    procedure EmitirDTE(const Factura: IPacioliDTE; out Mensaje: WideString); dispid 206;
    function NuevaFacturaExentaElectronica: IPacioliFacturaExentaElectronica; dispid 207;
    function NuevaGuiaDespachoElectronica: IPacioliGuiaDespachoElectronica; dispid 208;
    function NuevaNotaCreditoElectronica: IPacioliNotaCreditoElectronica; dispid 209;
    function ObtenerDTE(Folio: {??Int64}OleVariant; RutEmisor: SYSINT; TipoDocumento: SYSINT; 
                        out Mensaje: WideString): IPacioliDTE; dispid 210;
    function VerificarDTE(TipoDTE: SYSINT; Folio: {??Int64}OleVariant; out Mensaje: WideString): IPacioliEstadoDTE; dispid 211;
    function NuevaNotaDebitoElectronica: IPacioliNotaDebitoElectronica; dispid 212;
    function NuevaFacturaCompraElectronica: IPacioliFacturaCompraElectronica; dispid 213;
    function ObtenerDocumentos(Desde: TDateTime; Hasta: TDateTime): IPacioliListaDocumentos; dispid 1610612749;
    function ExportarXML(TipoDTE: SYSINT; FolioDTE: {??Int64}OleVariant; 
                         const OutputPath: WideString; out Mensaje: WideString): HResult; dispid 214;
    function ObtenerEmisor(Rut: SYSINT; out Mensaje: WideString): IPacioliEmpresa; dispid 215;
    function ExportarPDF32(TipoDTE: SYSINT; FolioDTE: SYSINT; const OutputPath: WideString; 
                           Cedible: WordBool; out Mensaje: WideString): HResult; dispid 216;
    function ObtenerDTE32(Folio: SYSINT; RutEmisor: SYSINT; TipoDocumento: SYSINT; 
                          out Mensaje: WideString): IPacioliDTE; dispid 217;
    function VerificarDTE32(TipoDTE: SYSINT; Folio: SYSINT; out Mensaje: WideString): IPacioliEstadoDTE; dispid 218;
    function ExportarXML32(TipoDTE: SYSINT; FolioDTE: SYSINT; const OutputPath: WideString; 
                           out Mensaje: WideString): HResult; dispid 219;
    function IniciarSesionEx(const Username: WideString; const Password: WideString; 
                             const ConfigDir: WideString; out Msg: WideString): WordBool; dispid 220;
  end;

// *********************************************************************//
// Interface: IPacioliDTE
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {933FC237-5477-4DED-B71E-A979657544CC}
// *********************************************************************//
  IPacioliDTE = interface(IDispatch)
    ['{933FC237-5477-4DED-B71E-A979657544CC}']
    function Get_Emisor: IPacioliEmpresa; safecall;
    procedure Set_Emisor(const Value: IPacioliEmpresa); safecall;
    function Get_Receptor: IPacioliEmpresa; safecall;
    procedure Set_Receptor(const Value: IPacioliEmpresa); safecall;
    function Get_Detalle: IPacioliDetalle; safecall;
    function NuevaLineaDetalle: IPacioliLineaDetalle; safecall;
    function Get_NumeroEnvio: Integer; safecall;
    function Get_Folio: Int64; safecall;
    function Get_FormaDePago: WideString; safecall;
    procedure Set_FormaDePago(const Value: WideString); safecall;
    function Get_Comentarios: WideString; safecall;
    procedure Set_Comentarios(const Value: WideString); safecall;
    function Get_DocumentosReferencia: IPacioliDocumentosReferencia; safecall;
    function NuevoDocumentoReferencia: IPacioliDocumentoReferencia; safecall;
    function Get_Folio32: Integer; safecall;
    procedure Set_Folio32(Value: Integer); safecall;
    function Get_Fecha: TDateTime; safecall;
    procedure Set_Fecha(Value: TDateTime); safecall;
    property Emisor: IPacioliEmpresa read Get_Emisor write Set_Emisor;
    property Receptor: IPacioliEmpresa read Get_Receptor write Set_Receptor;
    property Detalle: IPacioliDetalle read Get_Detalle;
    property NumeroEnvio: Integer read Get_NumeroEnvio;
    property Folio: Int64 read Get_Folio;
    property FormaDePago: WideString read Get_FormaDePago write Set_FormaDePago;
    property Comentarios: WideString read Get_Comentarios write Set_Comentarios;
    property DocumentosReferencia: IPacioliDocumentosReferencia read Get_DocumentosReferencia;
    property Folio32: Integer read Get_Folio32 write Set_Folio32;
    property Fecha: TDateTime read Get_Fecha write Set_Fecha;
  end;

// *********************************************************************//
// DispIntf:  IPacioliDTEDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {933FC237-5477-4DED-B71E-A979657544CC}
// *********************************************************************//
  IPacioliDTEDisp = dispinterface
    ['{933FC237-5477-4DED-B71E-A979657544CC}']
    property Emisor: IPacioliEmpresa dispid 201;
    property Receptor: IPacioliEmpresa dispid 202;
    property Detalle: IPacioliDetalle readonly dispid 203;
    function NuevaLineaDetalle: IPacioliLineaDetalle; dispid 204;
    property NumeroEnvio: Integer readonly dispid 205;
    property Folio: {??Int64}OleVariant readonly dispid 206;
    property FormaDePago: WideString dispid 207;
    property Comentarios: WideString dispid 208;
    property DocumentosReferencia: IPacioliDocumentosReferencia readonly dispid 209;
    function NuevoDocumentoReferencia: IPacioliDocumentoReferencia; dispid 210;
    property Folio32: Integer dispid 211;
    property Fecha: TDateTime dispid 212;
  end;

// *********************************************************************//
// Interface: IPacioliFacturaElectronica
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {60FB27A2-9F79-4F58-9767-E7CEB8D8F2FE}
// *********************************************************************//
  IPacioliFacturaElectronica = interface(IPacioliDTE)
    ['{60FB27A2-9F79-4F58-9767-E7CEB8D8F2FE}']
  end;

// *********************************************************************//
// DispIntf:  IPacioliFacturaElectronicaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {60FB27A2-9F79-4F58-9767-E7CEB8D8F2FE}
// *********************************************************************//
  IPacioliFacturaElectronicaDisp = dispinterface
    ['{60FB27A2-9F79-4F58-9767-E7CEB8D8F2FE}']
    property Emisor: IPacioliEmpresa dispid 201;
    property Receptor: IPacioliEmpresa dispid 202;
    property Detalle: IPacioliDetalle readonly dispid 203;
    function NuevaLineaDetalle: IPacioliLineaDetalle; dispid 204;
    property NumeroEnvio: Integer readonly dispid 205;
    property Folio: {??Int64}OleVariant readonly dispid 206;
    property FormaDePago: WideString dispid 207;
    property Comentarios: WideString dispid 208;
    property DocumentosReferencia: IPacioliDocumentosReferencia readonly dispid 209;
    function NuevoDocumentoReferencia: IPacioliDocumentoReferencia; dispid 210;
    property Folio32: Integer dispid 211;
    property Fecha: TDateTime dispid 212;
  end;

// *********************************************************************//
// Interface: IPacioliEmpresa
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {182BADE1-2ED1-4CDC-B010-8A89E4382CBE}
// *********************************************************************//
  IPacioliEmpresa = interface(IDispatch)
    ['{182BADE1-2ED1-4CDC-B010-8A89E4382CBE}']
    function Get_Rut: SYSINT; safecall;
    procedure Set_Rut(Value: SYSINT); safecall;
    function Get_RazonSocial: WideString; safecall;
    procedure Set_RazonSocial(const Value: WideString); safecall;
    function Get_Giro: WideString; safecall;
    procedure Set_Giro(const Value: WideString); safecall;
    function Get_Direccion: WideString; safecall;
    procedure Set_Direccion(const Value: WideString); safecall;
    function Get_Comuna: WideString; safecall;
    procedure Set_Comuna(const Value: WideString); safecall;
    function Get_Ciudad: WideString; safecall;
    procedure Set_Ciudad(const Value: WideString); safecall;
    function Get_Telefono: WideString; safecall;
    procedure Set_Telefono(const Value: WideString); safecall;
    function Get_EmailIntercambio: WideString; safecall;
    procedure Set_EmailIntercambio(const Value: WideString); safecall;
    property Rut: SYSINT read Get_Rut write Set_Rut;
    property RazonSocial: WideString read Get_RazonSocial write Set_RazonSocial;
    property Giro: WideString read Get_Giro write Set_Giro;
    property Direccion: WideString read Get_Direccion write Set_Direccion;
    property Comuna: WideString read Get_Comuna write Set_Comuna;
    property Ciudad: WideString read Get_Ciudad write Set_Ciudad;
    property Telefono: WideString read Get_Telefono write Set_Telefono;
    property EmailIntercambio: WideString read Get_EmailIntercambio write Set_EmailIntercambio;
  end;

// *********************************************************************//
// DispIntf:  IPacioliEmpresaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {182BADE1-2ED1-4CDC-B010-8A89E4382CBE}
// *********************************************************************//
  IPacioliEmpresaDisp = dispinterface
    ['{182BADE1-2ED1-4CDC-B010-8A89E4382CBE}']
    property Rut: SYSINT dispid 202;
    property RazonSocial: WideString dispid 201;
    property Giro: WideString dispid 203;
    property Direccion: WideString dispid 204;
    property Comuna: WideString dispid 205;
    property Ciudad: WideString dispid 206;
    property Telefono: WideString dispid 207;
    property EmailIntercambio: WideString dispid 208;
  end;

// *********************************************************************//
// Interface: IPacioliLineaDetalle
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AE212EE8-1C0F-4192-879B-318DD74B2A56}
// *********************************************************************//
  IPacioliLineaDetalle = interface(IDispatch)
    ['{AE212EE8-1C0F-4192-879B-318DD74B2A56}']
    function Get_TipoCodigo: WideString; safecall;
    procedure Set_TipoCodigo(const Value: WideString); safecall;
    function Get_ValorCodigo: WideString; safecall;
    procedure Set_ValorCodigo(const Value: WideString); safecall;
    function Get_Exento: WordBool; safecall;
    procedure Set_Exento(Value: WordBool); safecall;
    function Get_UnidadMedida: WideString; safecall;
    procedure Set_UnidadMedida(const Value: WideString); safecall;
    function Get_Precio: Currency; safecall;
    procedure Set_Precio(Value: Currency); safecall;
    function Get_Monto: Currency; safecall;
    procedure Set_Monto(Value: Currency); safecall;
    function Get_NombreItem: WideString; safecall;
    procedure Set_NombreItem(const Value: WideString); safecall;
    function Get_Impuestos: IPacioliImpuestos; safecall;
    function NuevoImpuesto: IPacioliImpuesto; safecall;
    function Get_PorcentajeDescuento: Double; safecall;
    procedure Set_PorcentajeDescuento(Value: Double); safecall;
    function Get_Cantidad: Double; safecall;
    procedure Set_Cantidad(Value: Double); safecall;
    function Get_DescripcionItem: WideString; safecall;
    procedure Set_DescripcionItem(const Value: WideString); safecall;
    function Get_Codigos: IPacioliCodigosDetalle; safecall;
    function NuevoCodigo: IPacioliCodigoDetalle; safecall;
    property TipoCodigo: WideString read Get_TipoCodigo write Set_TipoCodigo;
    property ValorCodigo: WideString read Get_ValorCodigo write Set_ValorCodigo;
    property Exento: WordBool read Get_Exento write Set_Exento;
    property UnidadMedida: WideString read Get_UnidadMedida write Set_UnidadMedida;
    property Precio: Currency read Get_Precio write Set_Precio;
    property Monto: Currency read Get_Monto write Set_Monto;
    property NombreItem: WideString read Get_NombreItem write Set_NombreItem;
    property Impuestos: IPacioliImpuestos read Get_Impuestos;
    property PorcentajeDescuento: Double read Get_PorcentajeDescuento write Set_PorcentajeDescuento;
    property Cantidad: Double read Get_Cantidad write Set_Cantidad;
    property DescripcionItem: WideString read Get_DescripcionItem write Set_DescripcionItem;
    property Codigos: IPacioliCodigosDetalle read Get_Codigos;
  end;

// *********************************************************************//
// DispIntf:  IPacioliLineaDetalleDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AE212EE8-1C0F-4192-879B-318DD74B2A56}
// *********************************************************************//
  IPacioliLineaDetalleDisp = dispinterface
    ['{AE212EE8-1C0F-4192-879B-318DD74B2A56}']
    property TipoCodigo: WideString dispid 201;
    property ValorCodigo: WideString dispid 202;
    property Exento: WordBool dispid 203;
    property UnidadMedida: WideString dispid 204;
    property Precio: Currency dispid 205;
    property Monto: Currency dispid 206;
    property NombreItem: WideString dispid 208;
    property Impuestos: IPacioliImpuestos readonly dispid 209;
    function NuevoImpuesto: IPacioliImpuesto; dispid 210;
    property PorcentajeDescuento: Double dispid 211;
    property Cantidad: Double dispid 207;
    property DescripcionItem: WideString dispid 212;
    property Codigos: IPacioliCodigosDetalle readonly dispid 213;
    function NuevoCodigo: IPacioliCodigoDetalle; dispid 214;
  end;

// *********************************************************************//
// Interface: IPacioliDetalle
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4BBB1169-670F-406A-B9FB-81F78A266F12}
// *********************************************************************//
  IPacioliDetalle = interface(IDispatch)
    ['{4BBB1169-670F-406A-B9FB-81F78A266F12}']
    function Get_Count: Integer; safecall;
    function Get_Item(Index: Integer): IPacioliLineaDetalle; safecall;
    property Count: Integer read Get_Count;
    property Item[Index: Integer]: IPacioliLineaDetalle read Get_Item;
  end;

// *********************************************************************//
// DispIntf:  IPacioliDetalleDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4BBB1169-670F-406A-B9FB-81F78A266F12}
// *********************************************************************//
  IPacioliDetalleDisp = dispinterface
    ['{4BBB1169-670F-406A-B9FB-81F78A266F12}']
    property Count: Integer readonly dispid 201;
    property Item[Index: Integer]: IPacioliLineaDetalle readonly dispid 202;
  end;

// *********************************************************************//
// Interface: IPacioliFacturaExentaElectronica
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {CEBA1B07-3066-4C4E-8FE5-B5B036F7867A}
// *********************************************************************//
  IPacioliFacturaExentaElectronica = interface(IPacioliDTE)
    ['{CEBA1B07-3066-4C4E-8FE5-B5B036F7867A}']
  end;

// *********************************************************************//
// DispIntf:  IPacioliFacturaExentaElectronicaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {CEBA1B07-3066-4C4E-8FE5-B5B036F7867A}
// *********************************************************************//
  IPacioliFacturaExentaElectronicaDisp = dispinterface
    ['{CEBA1B07-3066-4C4E-8FE5-B5B036F7867A}']
    property Emisor: IPacioliEmpresa dispid 201;
    property Receptor: IPacioliEmpresa dispid 202;
    property Detalle: IPacioliDetalle readonly dispid 203;
    function NuevaLineaDetalle: IPacioliLineaDetalle; dispid 204;
    property NumeroEnvio: Integer readonly dispid 205;
    property Folio: {??Int64}OleVariant readonly dispid 206;
    property FormaDePago: WideString dispid 207;
    property Comentarios: WideString dispid 208;
    property DocumentosReferencia: IPacioliDocumentosReferencia readonly dispid 209;
    function NuevoDocumentoReferencia: IPacioliDocumentoReferencia; dispid 210;
    property Folio32: Integer dispid 211;
    property Fecha: TDateTime dispid 212;
  end;

// *********************************************************************//
// Interface: IPacioliGuiaDespachoElectronica
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {75A5D31A-13C5-404F-8630-07EC90A90398}
// *********************************************************************//
  IPacioliGuiaDespachoElectronica = interface(IPacioliDTE)
    ['{75A5D31A-13C5-404F-8630-07EC90A90398}']
    function Get_TipoDespacho: Integer; safecall;
    procedure Set_TipoDespacho(Value: Integer); safecall;
    function Get_MotivoGuia: Integer; safecall;
    procedure Set_MotivoGuia(Value: Integer); safecall;
    property TipoDespacho: Integer read Get_TipoDespacho write Set_TipoDespacho;
    property MotivoGuia: Integer read Get_MotivoGuia write Set_MotivoGuia;
  end;

// *********************************************************************//
// DispIntf:  IPacioliGuiaDespachoElectronicaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {75A5D31A-13C5-404F-8630-07EC90A90398}
// *********************************************************************//
  IPacioliGuiaDespachoElectronicaDisp = dispinterface
    ['{75A5D31A-13C5-404F-8630-07EC90A90398}']
    property TipoDespacho: Integer dispid 301;
    property MotivoGuia: Integer dispid 302;
    property Emisor: IPacioliEmpresa dispid 201;
    property Receptor: IPacioliEmpresa dispid 202;
    property Detalle: IPacioliDetalle readonly dispid 203;
    function NuevaLineaDetalle: IPacioliLineaDetalle; dispid 204;
    property NumeroEnvio: Integer readonly dispid 205;
    property Folio: {??Int64}OleVariant readonly dispid 206;
    property FormaDePago: WideString dispid 207;
    property Comentarios: WideString dispid 208;
    property DocumentosReferencia: IPacioliDocumentosReferencia readonly dispid 209;
    function NuevoDocumentoReferencia: IPacioliDocumentoReferencia; dispid 210;
    property Folio32: Integer dispid 211;
    property Fecha: TDateTime dispid 212;
  end;

// *********************************************************************//
// Interface: IPacioliNotaCreditoElectronica
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {36FD0A7D-44A3-4DC4-B54B-C6025C536007}
// *********************************************************************//
  IPacioliNotaCreditoElectronica = interface(IPacioliDTE)
    ['{36FD0A7D-44A3-4DC4-B54B-C6025C536007}']
    function Get_TipoDocRef: Integer; safecall;
    procedure Set_TipoDocRef(Value: Integer); safecall;
    function Get_FolioRef: Int64; safecall;
    procedure Set_FolioRef(Value: Int64); safecall;
    function Get_MotivoNota: Integer; safecall;
    procedure Set_MotivoNota(Value: Integer); safecall;
    function Get_FolioRef32: Integer; safecall;
    procedure Set_FolioRef32(Value: Integer); safecall;
    function Get_FechaReferencia: TDateTime; safecall;
    procedure Set_FechaReferencia(Value: TDateTime); safecall;
    property TipoDocRef: Integer read Get_TipoDocRef write Set_TipoDocRef;
    property FolioRef: Int64 read Get_FolioRef write Set_FolioRef;
    property MotivoNota: Integer read Get_MotivoNota write Set_MotivoNota;
    property FolioRef32: Integer read Get_FolioRef32 write Set_FolioRef32;
    property FechaReferencia: TDateTime read Get_FechaReferencia write Set_FechaReferencia;
  end;

// *********************************************************************//
// DispIntf:  IPacioliNotaCreditoElectronicaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {36FD0A7D-44A3-4DC4-B54B-C6025C536007}
// *********************************************************************//
  IPacioliNotaCreditoElectronicaDisp = dispinterface
    ['{36FD0A7D-44A3-4DC4-B54B-C6025C536007}']
    property TipoDocRef: Integer dispid 301;
    property FolioRef: {??Int64}OleVariant dispid 302;
    property MotivoNota: Integer dispid 305;
    property FolioRef32: Integer dispid 303;
    property FechaReferencia: TDateTime dispid 304;
    property Emisor: IPacioliEmpresa dispid 201;
    property Receptor: IPacioliEmpresa dispid 202;
    property Detalle: IPacioliDetalle readonly dispid 203;
    function NuevaLineaDetalle: IPacioliLineaDetalle; dispid 204;
    property NumeroEnvio: Integer readonly dispid 205;
    property Folio: {??Int64}OleVariant readonly dispid 206;
    property FormaDePago: WideString dispid 207;
    property Comentarios: WideString dispid 208;
    property DocumentosReferencia: IPacioliDocumentosReferencia readonly dispid 209;
    function NuevoDocumentoReferencia: IPacioliDocumentoReferencia; dispid 210;
    property Folio32: Integer dispid 211;
    property Fecha: TDateTime dispid 212;
  end;

// *********************************************************************//
// Interface: IPacioliImpuesto
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8CC23941-B1CB-4B0A-9B4B-137089628C30}
// *********************************************************************//
  IPacioliImpuesto = interface(IDispatch)
    ['{8CC23941-B1CB-4B0A-9B4B-137089628C30}']
    function Get_Codigo: Integer; safecall;
    procedure Set_Codigo(Value: Integer); safecall;
    function Get_Tasa: Double; safecall;
    procedure Set_Tasa(Value: Double); safecall;
    property Codigo: Integer read Get_Codigo write Set_Codigo;
    property Tasa: Double read Get_Tasa write Set_Tasa;
  end;

// *********************************************************************//
// DispIntf:  IPacioliImpuestoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8CC23941-B1CB-4B0A-9B4B-137089628C30}
// *********************************************************************//
  IPacioliImpuestoDisp = dispinterface
    ['{8CC23941-B1CB-4B0A-9B4B-137089628C30}']
    property Codigo: Integer dispid 201;
    property Tasa: Double dispid 202;
  end;

// *********************************************************************//
// Interface: IPacioliImpuestos
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FFCBAB83-588C-4BC7-86B9-ACAEA999F887}
// *********************************************************************//
  IPacioliImpuestos = interface(IDispatch)
    ['{FFCBAB83-588C-4BC7-86B9-ACAEA999F887}']
    function Get_Count: Integer; safecall;
    function Get_Item(Index: Integer): IPacioliImpuesto; safecall;
    property Count: Integer read Get_Count;
    property Item[Index: Integer]: IPacioliImpuesto read Get_Item;
  end;

// *********************************************************************//
// DispIntf:  IPacioliImpuestosDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FFCBAB83-588C-4BC7-86B9-ACAEA999F887}
// *********************************************************************//
  IPacioliImpuestosDisp = dispinterface
    ['{FFCBAB83-588C-4BC7-86B9-ACAEA999F887}']
    property Count: Integer readonly dispid 201;
    property Item[Index: Integer]: IPacioliImpuesto readonly dispid 202;
  end;

// *********************************************************************//
// Interface: IPacioliEstadoDTE
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {36DDEB2D-AD57-4506-B8B6-799ABADDA8EF}
// *********************************************************************//
  IPacioliEstadoDTE = interface(IDispatch)
    ['{36DDEB2D-AD57-4506-B8B6-799ABADDA8EF}']
    function Get_ErrCode: Integer; safecall;
    procedure Set_ErrCode(Value: Integer); safecall;
    function Get_Estado: WideString; safecall;
    procedure Set_Estado(const Value: WideString); safecall;
    function Get_GlosaEstado: WideString; safecall;
    procedure Set_GlosaEstado(const Value: WideString); safecall;
    function Get_GlosaErr: WideString; safecall;
    procedure Set_GlosaErr(const Value: WideString); safecall;
    function Get_NumAtencion: WideString; safecall;
    procedure Set_NumAtencion(const Value: WideString); safecall;
    function Get_RawResponse: WideString; safecall;
    procedure Set_RawResponse(const Value: WideString); safecall;
    property ErrCode: Integer read Get_ErrCode write Set_ErrCode;
    property Estado: WideString read Get_Estado write Set_Estado;
    property GlosaEstado: WideString read Get_GlosaEstado write Set_GlosaEstado;
    property GlosaErr: WideString read Get_GlosaErr write Set_GlosaErr;
    property NumAtencion: WideString read Get_NumAtencion write Set_NumAtencion;
    property RawResponse: WideString read Get_RawResponse write Set_RawResponse;
  end;

// *********************************************************************//
// DispIntf:  IPacioliEstadoDTEDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {36DDEB2D-AD57-4506-B8B6-799ABADDA8EF}
// *********************************************************************//
  IPacioliEstadoDTEDisp = dispinterface
    ['{36DDEB2D-AD57-4506-B8B6-799ABADDA8EF}']
    property ErrCode: Integer dispid 203;
    property Estado: WideString dispid 201;
    property GlosaEstado: WideString dispid 202;
    property GlosaErr: WideString dispid 204;
    property NumAtencion: WideString dispid 205;
    property RawResponse: WideString dispid 206;
  end;

// *********************************************************************//
// Interface: IPacioliDocumentoReferencia
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {44CE42A0-0BCC-4009-B419-1F6A1E161B6A}
// *********************************************************************//
  IPacioliDocumentoReferencia = interface(IDispatch)
    ['{44CE42A0-0BCC-4009-B419-1F6A1E161B6A}']
    function Get_TipoDocumento: WideString; safecall;
    procedure Set_TipoDocumento(const Value: WideString); safecall;
    function Get_Folio: WideString; safecall;
    procedure Set_Folio(const Value: WideString); safecall;
    function Get_FechaReferencia: TDateTime; safecall;
    procedure Set_FechaReferencia(Value: TDateTime); safecall;
    function Get_RazonReferencia: WideString; safecall;
    procedure Set_RazonReferencia(const Value: WideString); safecall;
    property TipoDocumento: WideString read Get_TipoDocumento write Set_TipoDocumento;
    property Folio: WideString read Get_Folio write Set_Folio;
    property FechaReferencia: TDateTime read Get_FechaReferencia write Set_FechaReferencia;
    property RazonReferencia: WideString read Get_RazonReferencia write Set_RazonReferencia;
  end;

// *********************************************************************//
// DispIntf:  IPacioliDocumentoReferenciaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {44CE42A0-0BCC-4009-B419-1F6A1E161B6A}
// *********************************************************************//
  IPacioliDocumentoReferenciaDisp = dispinterface
    ['{44CE42A0-0BCC-4009-B419-1F6A1E161B6A}']
    property TipoDocumento: WideString dispid 201;
    property Folio: WideString dispid 202;
    property FechaReferencia: TDateTime dispid 203;
    property RazonReferencia: WideString dispid 204;
  end;

// *********************************************************************//
// Interface: IPacioliDocumentosReferencia
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E9732759-C1D5-448E-8B74-D159318930C2}
// *********************************************************************//
  IPacioliDocumentosReferencia = interface(IDispatch)
    ['{E9732759-C1D5-448E-8B74-D159318930C2}']
    function Get_Count: Integer; safecall;
    function Get_Item(Index: Integer): IPacioliDocumentoReferencia; safecall;
    property Count: Integer read Get_Count;
    property Item[Index: Integer]: IPacioliDocumentoReferencia read Get_Item;
  end;

// *********************************************************************//
// DispIntf:  IPacioliDocumentosReferenciaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E9732759-C1D5-448E-8B74-D159318930C2}
// *********************************************************************//
  IPacioliDocumentosReferenciaDisp = dispinterface
    ['{E9732759-C1D5-448E-8B74-D159318930C2}']
    property Count: Integer readonly dispid 201;
    property Item[Index: Integer]: IPacioliDocumentoReferencia readonly dispid 202;
  end;

// *********************************************************************//
// Interface: IPacioliNotaDebitoElectronica
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C4DA045A-7330-40C4-9F08-F84CE83D5AC5}
// *********************************************************************//
  IPacioliNotaDebitoElectronica = interface(IPacioliNotaCreditoElectronica)
    ['{C4DA045A-7330-40C4-9F08-F84CE83D5AC5}']
  end;

// *********************************************************************//
// DispIntf:  IPacioliNotaDebitoElectronicaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C4DA045A-7330-40C4-9F08-F84CE83D5AC5}
// *********************************************************************//
  IPacioliNotaDebitoElectronicaDisp = dispinterface
    ['{C4DA045A-7330-40C4-9F08-F84CE83D5AC5}']
    property TipoDocRef: Integer dispid 301;
    property FolioRef: {??Int64}OleVariant dispid 302;
    property MotivoNota: Integer dispid 305;
    property FolioRef32: Integer dispid 303;
    property FechaReferencia: TDateTime dispid 304;
    property Emisor: IPacioliEmpresa dispid 201;
    property Receptor: IPacioliEmpresa dispid 202;
    property Detalle: IPacioliDetalle readonly dispid 203;
    function NuevaLineaDetalle: IPacioliLineaDetalle; dispid 204;
    property NumeroEnvio: Integer readonly dispid 205;
    property Folio: {??Int64}OleVariant readonly dispid 206;
    property FormaDePago: WideString dispid 207;
    property Comentarios: WideString dispid 208;
    property DocumentosReferencia: IPacioliDocumentosReferencia readonly dispid 209;
    function NuevoDocumentoReferencia: IPacioliDocumentoReferencia; dispid 210;
    property Folio32: Integer dispid 211;
    property Fecha: TDateTime dispid 212;
  end;

// *********************************************************************//
// Interface: IPacioliFacturaCompraElectronica
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3D258DD8-359A-4D7C-A122-F0565F12E300}
// *********************************************************************//
  IPacioliFacturaCompraElectronica = interface(IPacioliDTE)
    ['{3D258DD8-359A-4D7C-A122-F0565F12E300}']
  end;

// *********************************************************************//
// DispIntf:  IPacioliFacturaCompraElectronicaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3D258DD8-359A-4D7C-A122-F0565F12E300}
// *********************************************************************//
  IPacioliFacturaCompraElectronicaDisp = dispinterface
    ['{3D258DD8-359A-4D7C-A122-F0565F12E300}']
    property Emisor: IPacioliEmpresa dispid 201;
    property Receptor: IPacioliEmpresa dispid 202;
    property Detalle: IPacioliDetalle readonly dispid 203;
    function NuevaLineaDetalle: IPacioliLineaDetalle; dispid 204;
    property NumeroEnvio: Integer readonly dispid 205;
    property Folio: {??Int64}OleVariant readonly dispid 206;
    property FormaDePago: WideString dispid 207;
    property Comentarios: WideString dispid 208;
    property DocumentosReferencia: IPacioliDocumentosReferencia readonly dispid 209;
    function NuevoDocumentoReferencia: IPacioliDocumentoReferencia; dispid 210;
    property Folio32: Integer dispid 211;
    property Fecha: TDateTime dispid 212;
  end;

// *********************************************************************//
// Interface: IPacioliListaDocumentos
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E661F9AC-927E-4C52-AB6C-F54B2FEC5C8B}
// *********************************************************************//
  IPacioliListaDocumentos = interface(IDispatch)
    ['{E661F9AC-927E-4C52-AB6C-F54B2FEC5C8B}']
    function Get_Count: Integer; safecall;
    function Get_Item(Index: Integer): IPacioliResumenDocumento; safecall;
    property Count: Integer read Get_Count;
    property Item[Index: Integer]: IPacioliResumenDocumento read Get_Item;
  end;

// *********************************************************************//
// DispIntf:  IPacioliListaDocumentosDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E661F9AC-927E-4C52-AB6C-F54B2FEC5C8B}
// *********************************************************************//
  IPacioliListaDocumentosDisp = dispinterface
    ['{E661F9AC-927E-4C52-AB6C-F54B2FEC5C8B}']
    property Count: Integer readonly dispid 201;
    property Item[Index: Integer]: IPacioliResumenDocumento readonly dispid 202;
  end;

// *********************************************************************//
// Interface: IPacioliResumenDocumento
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {005352D9-08A1-42A6-AD83-DEDBEA738592}
// *********************************************************************//
  IPacioliResumenDocumento = interface(IDispatch)
    ['{005352D9-08A1-42A6-AD83-DEDBEA738592}']
    function Get_ID: Integer; safecall;
    function Get_TipoDocumento: WideString; safecall;
    function Get_Folio: Int64; safecall;
    function Get_RutEmisor: SYSINT; safecall;
    function Get_FechaEmision: TDateTime; safecall;
    function Get_Total: Currency; safecall;
    function Get_IVA: Currency; safecall;
    property ID: Integer read Get_ID;
    property TipoDocumento: WideString read Get_TipoDocumento;
    property Folio: Int64 read Get_Folio;
    property RutEmisor: SYSINT read Get_RutEmisor;
    property FechaEmision: TDateTime read Get_FechaEmision;
    property Total: Currency read Get_Total;
    property IVA: Currency read Get_IVA;
  end;

// *********************************************************************//
// DispIntf:  IPacioliResumenDocumentoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {005352D9-08A1-42A6-AD83-DEDBEA738592}
// *********************************************************************//
  IPacioliResumenDocumentoDisp = dispinterface
    ['{005352D9-08A1-42A6-AD83-DEDBEA738592}']
    property ID: Integer readonly dispid 201;
    property TipoDocumento: WideString readonly dispid 202;
    property Folio: {??Int64}OleVariant readonly dispid 203;
    property RutEmisor: SYSINT readonly dispid 204;
    property FechaEmision: TDateTime readonly dispid 205;
    property Total: Currency readonly dispid 206;
    property IVA: Currency readonly dispid 207;
  end;

// *********************************************************************//
// Interface: IPacioliCodigosDetalle
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4DBE137B-BA58-470C-84A2-BFA4AB9A01CD}
// *********************************************************************//
  IPacioliCodigosDetalle = interface(IDispatch)
    ['{4DBE137B-BA58-470C-84A2-BFA4AB9A01CD}']
    function Get_Count: Integer; safecall;
    function Get_Item(Index: Integer): IPacioliCodigoDetalle; safecall;
    property Count: Integer read Get_Count;
    property Item[Index: Integer]: IPacioliCodigoDetalle read Get_Item;
  end;

// *********************************************************************//
// DispIntf:  IPacioliCodigosDetalleDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4DBE137B-BA58-470C-84A2-BFA4AB9A01CD}
// *********************************************************************//
  IPacioliCodigosDetalleDisp = dispinterface
    ['{4DBE137B-BA58-470C-84A2-BFA4AB9A01CD}']
    property Count: Integer readonly dispid 201;
    property Item[Index: Integer]: IPacioliCodigoDetalle readonly dispid 202;
  end;

// *********************************************************************//
// Interface: IPacioliCodigoDetalle
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B05C8934-6F4A-4F78-A064-262ED5767D33}
// *********************************************************************//
  IPacioliCodigoDetalle = interface(IDispatch)
    ['{B05C8934-6F4A-4F78-A064-262ED5767D33}']
    function Get_TipoCodigo: WideString; safecall;
    procedure Set_TipoCodigo(const Value: WideString); safecall;
    function Get_ValorCodigo: WideString; safecall;
    procedure Set_ValorCodigo(const Value: WideString); safecall;
    property TipoCodigo: WideString read Get_TipoCodigo write Set_TipoCodigo;
    property ValorCodigo: WideString read Get_ValorCodigo write Set_ValorCodigo;
  end;

// *********************************************************************//
// DispIntf:  IPacioliCodigoDetalleDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B05C8934-6F4A-4F78-A064-262ED5767D33}
// *********************************************************************//
  IPacioliCodigoDetalleDisp = dispinterface
    ['{B05C8934-6F4A-4F78-A064-262ED5767D33}']
    property TipoCodigo: WideString dispid 201;
    property ValorCodigo: WideString dispid 202;
  end;

// *********************************************************************//
// The Class CoPacioliCOMObj provides a Create and CreateRemote method to          
// create instances of the default interface IPacioliCOMObj exposed by              
// the CoClass PacioliCOMObj. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoPacioliCOMObj = class
    class function Create: IPacioliCOMObj;
    class function CreateRemote(const MachineName: string): IPacioliCOMObj;
  end;

implementation

uses ComObj;

class function CoPacioliCOMObj.Create: IPacioliCOMObj;
begin
  Result := CreateComObject(CLASS_PacioliCOMObj) as IPacioliCOMObj;
end;

class function CoPacioliCOMObj.CreateRemote(const MachineName: string): IPacioliCOMObj;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_PacioliCOMObj) as IPacioliCOMObj;
end;

end.
