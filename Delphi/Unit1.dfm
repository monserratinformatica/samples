object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 240
  ClientWidth = 496
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    496
    240)
  PixelsPerInch = 96
  TextHeight = 13
  object lblPerfil: TLabel
    Left = 8
    Top = 40
    Width = 28
    Height = 13
    Caption = 'Perfil:'
  end
  object lblUsername: TLabel
    Left = 8
    Top = 70
    Width = 52
    Height = 13
    Caption = 'Username:'
    FocusControl = editUsername
  end
  object lblPassword: TLabel
    Left = 8
    Top = 97
    Width = 50
    Height = 13
    Caption = 'Password:'
    FocusControl = editPassword
  end
  object cbConfiguracionesActivas: TComboBox
    Left = 88
    Top = 40
    Width = 400
    Height = 21
    Style = csDropDownList
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    OnChange = cbConfiguracionesActivasChange
  end
  object editUsername: TEdit
    Left = 88
    Top = 67
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object editPassword: TEdit
    Left = 88
    Top = 94
    Width = 121
    Height = 21
    PasswordChar = '*'
    TabOrder = 2
  end
  object btnIniciaSesion: TButton
    Left = 413
    Top = 92
    Width = 75
    Height = 25
    Caption = 'Login'
    Enabled = False
    TabOrder = 3
    OnClick = btnIniciaSesionClick
  end
  object btnEmitirFactura: TButton
    Left = 192
    Top = 192
    Width = 105
    Height = 25
    Caption = 'Emitir Factura'
    Enabled = False
    TabOrder = 4
    OnClick = btnEmitirFacturaClick
  end
end
